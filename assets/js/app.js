 console.info("Vendo nossos códigos? Trabalhe conosco!");

 $(document).on('ready', function () {
 	var winHeight = $(window).height(),
 	docHeight = $(document).height(),
 	progressBar = $('progress'),
 	max, value;
 	max = docHeight - winHeight;
 	progressBar.attr('max', max);
 	$(document).on('scroll', function () {
 		value = $(window).scrollTop();
 		progressBar.attr('value', value);
 		if (value > 0) {
 			$("#to-top").show("slow");
 			$("#header").attr("style", "background:#fff;box-shadow:0 3px 3px rgba(0,0,0,0.3)");
 			$("#header a").attr("style", "color:#236e47");
 		} else {
 			$("#to-top").hide("slow");
 			$("#header").attr("style", "");
 			$("#header a").attr("style", "");
 		}
 	});

 	$('.collapsed').on('click', function () {
 		$("#header").attr("style", "background:#fff;box-shadow:0 3px 3px rgba(0,0,0,0.3)");
 		$("#header a").attr("style", "color:#236e47");
 	}
 	);


 	/* MENU TOPO */
 	function filterPath(string) {
 		return string.replace(/^\//, '').replace(/(index|default).[a-zA-Z]{3,4}$/, '').replace(/\/$/, '');
 	}
 	$('a.menu-link').each(function () {
 		if (filterPath(location.pathname) == filterPath(this.pathname) && location.hostname == this.hostname && this.hash.replace(/#/, '')) {
 			var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) + ']');
 			var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
 			if ($target) {
 				var targetOffset = $target.offset().top - 0;
 				$(this).click(function () {
 					$('html, body').animate({scrollTop: targetOffset}, 900);
 					return false;
 				});
 			}
 		}
 	});   
 });

 function getAnos() {
 	 /* Primeiro ano da AJE-MS */
    var s_year = 2011;
    /* Ano atual */
    var a_year = new Date().getYear() + 1900;
    var years = (s_year == a_year) ? s_year : s_year + " &mdash; " + a_year;

    return years;
 }