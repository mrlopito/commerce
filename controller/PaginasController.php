<?php
    namespace App\Controller;
    use Slim\Views\PhpRenderer as View;

    class PaginasController{
        private $view;
        function __construct(View $view)
        {
            $this->view = $view;
        }
        public function index($request, $response){

            return $this->view->render($response, '/Loja/index.phtml');
        }
    }


?>