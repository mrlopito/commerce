<?php
    namespace App\Controller;
    use Slim\Views\PhpRenderer as View;
    class ApolloController{
        private $view;
        function __construct(View $view){
            $this->view = $view;
        }
        public function index($request, $response){
            return $this->view->render($response, '/Apollo/index.phtml');
        }


    }

?>