<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($container) {
    $settings = $container->get('settings')['renderer'];
    $capsule = new Slim\Views\PhpRenderer($settings['template_path']);
    return $capsule;
};

// PagesController
$container['PaginasController'] = function ($container)
{
    return new App\Controller\PaginasController($container->renderer);
};
$container['ApolloController'] = function ($container)
{
    return new App\Controller\ApolloController($container->renderer);
};