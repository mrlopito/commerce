<?php

require __DIR__ . '/vendor/autoload.php';

session_start();

$settings = require __DIR__ . '/config/settings.php';
$app = new Slim\App($settings);

// Eloquent Database
$container = $app->getContainer();
$capsule = new Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container->get('settings')['db']);

$capsule->setAsGlobal();
$capsule->bootEloquent();

// Set up dependencies
require __DIR__ . '/config/dependencies.php';

// Register routes
require __DIR__ . '/config/routes.php';

// Run app
$app->run();